<?php

namespace APP\SummaryOfOrganization;
use App\Model\Database as DB;
require_once("../../../vendor/autoload.php");
use PDO;
class SummaryOfOrganization extends DB
{
    public $id;
    public $organization_name;
    public $organization_summary;
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        echo "inside index of summary organization";
    }
}
$obj = new SummaryOfOrganization();
